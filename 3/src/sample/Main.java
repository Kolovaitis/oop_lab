package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import sample.controller.Controller;
import sample.dataWorker.DataWorker;
import sample.dataWorker.FileDataWorker;
import sample.model.ObjectManager;
import sample.model.StoredObjectManager;
import sample.serialize.BSONSerializer;
import sample.serialize.BinarySerializer;
import sample.view.FieldsView;
import sample.view.View;

import java.io.File;


public class Main extends Application {
public static final String DEFAULT_FILE = "test.dat";
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader();
        Parent root = loader.load(getClass().getResource("layout/sample.fxml").openStream());
        primaryStage.setTitle("Lab_3");
        primaryStage.setScene(new Scene(root, 800, 480));
        primaryStage.setResizable(false);
        primaryStage.show();
        Controller controller = loader.getController();
        DataWorker dataWorker = new FileDataWorker(new File(DEFAULT_FILE));
        ObjectManager model = new StoredObjectManager(dataWorker, new BinarySerializer());
        controller.setModel(model);
        View view = new View(root, model);
        model.addObserver(view);
        FieldsView fieldsView = new FieldsView((Pane) root.lookup("#fields_container"), model, view);
        model.addObserver(fieldsView);
    }


    public static void main(String[] args) {
     launch(args);
    }
}
