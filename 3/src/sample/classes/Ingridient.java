package sample.classes;

import sample.Type;
import sample.classes.view.FieldsClassCreator;
import sample.classes.view.IngridientViewCreator;

public abstract class Ingridient extends Food {
    public Ingridient(Type type) {
        super(type);
    }
    @Override
    public FieldsClassCreator getFieldsCreator() {
        return new IngridientViewCreator(this);
    }
    @Override
    public boolean hasIngridient(Food object) {
        return false;
    }

    @Override
    public void removeIngridient(Food object) {

    }

}
