package sample.classes.view;

import javafx.scene.Node;
import sample.model.ObjectManager;
import sample.view.View;

public interface FieldsClassCreator {
    Node createFields(ObjectManager model, View itemsView);

}
