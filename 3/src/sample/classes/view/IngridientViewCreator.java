package sample.classes.view;

import javafx.beans.property.StringProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import sample.Main;
import sample.classes.Ingridient;
import sample.model.ObjectManager;
import sample.view.View;

import java.io.IOException;

public class IngridientViewCreator implements FieldsClassCreator {
    private Ingridient ingridient;

    public IngridientViewCreator(Ingridient ingridient) {
        this.ingridient = ingridient;
    }

    @Override
    public Node createFields(ObjectManager model, View itemsView) {
        try {
            Node itemNode = FXMLLoader.load(Main.class.getResource("layout/ingridient.fxml"));
            setupName(ingridient, model, itemsView, itemNode);
            setupType(itemNode);
            setupDeleteButton(model, itemNode);
            return itemNode;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setupDeleteButton(ObjectManager model, Node itemNode) {
        Button delete = (Button)itemNode.lookup("#delete");
        delete.setOnMouseClicked((mouseEvent)->{
            model.removeObject(ingridient);
        });
    }

    private void setupType(Node itemNode) {
        StringProperty type = ((Label) itemNode.lookup("#product_type")).textProperty();
        type.set(ingridient.getType().getName());
    }

    private void setupName(Ingridient item, ObjectManager model, View itemsView, Node itemNode) {
        StringProperty name = ((TextField) itemNode.lookup("#name")).textProperty();
        name.set(item.getName());
        name.addListener(changeListener->{
            item.setName(name.get());
            model.notifyObserver(itemsView);
        });
    }
}
