package sample.classes.view;

import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import sample.Main;
import sample.Type;
import sample.classes.Ingridient;
import sample.classes.Pizza;
import sample.model.ObjectManager;
import sample.view.View;

import java.io.IOException;

public class PizzaViewCreator implements FieldsClassCreator {
    private Pizza pizza;

    public PizzaViewCreator(Pizza pizza) {
        this.pizza = pizza;
    }

    @Override
    public Node createFields(ObjectManager model, View itemsView) {
        try {
            Node itemNode = FXMLLoader.load(Main.class.getResource("layout/pizza.fxml"));
            setupName(pizza, model, itemsView, itemNode);
            setupIngridient(model, itemNode, pizza, Type.CHEESE);
            setupIngridient(model, itemNode, pizza, Type.MEAT);
            setupIngridient(model, itemNode, pizza, Type.SOUS);
            setupIngridient(model, itemNode, pizza, Type.VEGETABLE);
            setupDeleteButton(model, itemNode);
            return itemNode;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void setupIngridient(ObjectManager model, Node itemNode, Pizza pizza, Type type) {
        ChoiceBox ingridient = (ChoiceBox) itemNode.lookup("#" + type.getId());
        ingridient.itemsProperty().set(FXCollections.observableList(model.getObjectsByType(type)));
        ingridient.getSelectionModel().select(pizza.getIngridient(type));
        ingridient.getSelectionModel().selectedItemProperty().addListener((observable,
                                                                           oldValue, newValue) -> {
            pizza.setIngridient(type, (Ingridient) newValue);
        });
    }

    private void setupName(Pizza item, ObjectManager model, View itemsView, Node itemNode) {
        StringProperty name = ((TextField) itemNode.lookup("#name")).textProperty();
        name.set(item.getName());
        name.addListener(changeListener -> {
            item.setName(name.get());
            model.notifyObserver(itemsView);
        });
    }

    private void setupDeleteButton(ObjectManager model, Node itemNode) {
        Button delete = (Button) itemNode.lookup("#delete");
        delete.setOnMouseClicked((mouseEvent) -> {
            model.removeObject(pizza);
        });
    }

}
