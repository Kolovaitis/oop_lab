package sample.classes;

import sample.Type;

public class Meat extends Ingridient {
    public Meat(Type type) {
        super(type);
    }
}
