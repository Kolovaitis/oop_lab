package sample.classes;

import org.bson.BSONObject;
import sample.Type;
import sample.classes.view.FieldsClassCreator;
import sample.view.ViewCreator;

import java.io.Serializable;

public abstract class Food implements Serializable {
    private boolean isChoosed;
    private Type type;
    private String name;
    public Food(Type type, String name){
        this.type = type;
        this.name = name;
        this.isChoosed = false;
    }

    public Food(Type type) {
        this.type = type;
        this.name = "name";
        this.isChoosed = false;
    }

    public boolean isChoosed() {
        return isChoosed;
    }

    public void setChoosed(boolean choosed) {
        isChoosed = choosed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return name;
    }
    public abstract FieldsClassCreator getFieldsCreator();

    public abstract boolean hasIngridient(Food object);
    public abstract void removeIngridient(Food object);
}
