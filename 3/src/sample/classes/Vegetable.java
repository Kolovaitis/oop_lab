package sample.classes;

import sample.Type;

public class Vegetable extends Ingridient {
    public Vegetable(Type type) {
        super(type);
    }
}
