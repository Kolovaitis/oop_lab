package sample.classes;

import sample.Type;
import sample.classes.view.FieldsClassCreator;
import sample.classes.view.PizzaViewCreator;

public class Pizza extends Food {
private Cheese cheese;
private Meat meat;
private Vegetable vegetable;
private Sous sous;

    public Sous getSouse() {
        return sous;
    }

    public void setSouse(Sous sous) {
        this.sous = sous;
    }

    public Pizza(Type type) {
        super(type);
    }

    @Override
    public FieldsClassCreator getFieldsCreator() {
        return new PizzaViewCreator(this);
    }

    @Override
    public boolean hasIngridient(Food object) {
        if(object==cheese||object==meat||object==vegetable||object==sous){
            return true;
        }
        return false;
    }

    @Override
    public void removeIngridient(Food object) {
        setIngridient(object.getType(), null);
    }

    public Cheese getCheese() {
        return cheese;
    }

    public void setCheese(Cheese cheese) {
        this.cheese = cheese;
    }

    public Meat getMeat() {
        return meat;
    }

    public void setMeat(Meat meat) {
        this.meat = meat;
    }

    public Vegetable getVegetable() {
        return vegetable;
    }

    public void setVegetable(Vegetable vegetable) {
        this.vegetable = vegetable;
    }

    public Ingridient getIngridient(Type type) {
        switch (type){
            case CHEESE:return cheese;
            case MEAT:return meat;
            case SOUS:return sous;
            case VEGETABLE: return vegetable;
        }
        return null;
    }

    public void setIngridient(Type type, Ingridient newValue) {
        switch (type){
            case CHEESE:cheese = (Cheese) newValue;break;
            case MEAT:meat = (Meat)newValue;break;
            case SOUS:sous = (Sous)newValue;break;
            case VEGETABLE: vegetable = (Vegetable)newValue;break;
        }
    }
}
