package sample.classes;

import sample.Type;

public class Cheese extends Ingridient{
    public Cheese(Type type) {
        super(type);
    }
}
