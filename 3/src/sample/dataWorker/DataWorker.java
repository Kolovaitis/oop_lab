package sample.dataWorker;

import java.io.InputStream;
import java.io.OutputStream;

public interface DataWorker {
    InputStream getInputStream();
    OutputStream getOutputStream();
}
