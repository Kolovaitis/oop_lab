package sample.model;

import sample.Type;
import sample.classes.Ingridient;
import sample.dataWorker.DataWorker;
import sample.serialize.Serializer;
import sample.classes.Food;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class StoredObjectManager implements ObjectManager {
    private List<Food> objects;
    private DataWorker dataWorker;
    private Serializer serializer;
    private Food choosedObject;

    public StoredObjectManager(DataWorker dataWorker, Serializer serializer) {
        this.dataWorker = dataWorker;
        this.serializer = serializer;
        objects = serializer.deserialize(dataWorker.getInputStream());
    }

    @Override
    public void addObject(Food object) {
        objects.add(object);
        notifyObservers();
    }

    @Override
    public List<Food> getAllObjects() {
        return objects;
    }


    @Override
    public void removeObject(Food object) {
        for (Food food : objects) {
            if (!food.getType().isIngridient()) {
                if (food.hasIngridient(object)) {
                    food.removeIngridient(object);
                }
            }
        }
        if(choosedObject == object){
            choosedObject = null;
        }
        objects.remove(object);
        notifyObservers();
    }


    @Override
    public void sortObjects(Comparator comparator) {
        objects.sort(comparator);
        notifyObservers();
    }

    @Override
    public Food getChoosedObject() {
        return choosedObject;
    }

    @Override
    public void setChoosedObject(Food choosedObject) {
        if (this.choosedObject != null) {
            this.choosedObject.setChoosed(false);
        }
        this.choosedObject = choosedObject;
        this.choosedObject.setChoosed(true);
        notifyObservers();
    }

    @Override
    public List<Food> getObjectsByType(Type type) {
        List<Food> objectsByType = new ArrayList<>();
        for (Food item : objects) {
            if (item.getType().equals(type)) {
                objectsByType.add(item);
            }
        }
        return objectsByType;
    }

    @Override
    public List<Ingridient> getIngridients() {
        List<Ingridient> ingridients = new ArrayList<>();
        for (Food item : objects) {
            if (item.getType().isIngridient()) {
                ingridients.add((Ingridient) item);
            }
        }
        return ingridients;
    }

    public void saveObjects() {
        if (choosedObject != null)
            this.choosedObject.setChoosed(false);
        serializer.serialize(objects, dataWorker.getOutputStream());
        if (choosedObject != null)
            this.choosedObject.setChoosed(true);
    }
}
