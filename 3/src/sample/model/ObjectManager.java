package sample.model;


import sample.Type;
import sample.classes.Ingridient;
import sample.observe.Observable;
import sample.classes.Food;

import java.util.Comparator;
import java.util.List;

public interface ObjectManager extends Observable {
    void addObject(Food object);
    List<Food> getAllObjects();
    void removeObject(Food object);
    void sortObjects(Comparator comparator);
    Food getChoosedObject();
    void setChoosedObject(Food object);

    List<Food> getObjectsByType(Type type);

    List<Ingridient> getIngridients();
}
