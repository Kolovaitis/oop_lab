package sample.creator;

import sample.Type;
import sample.classes.Food;

public interface Creator {
    Food create(Type type);
}
