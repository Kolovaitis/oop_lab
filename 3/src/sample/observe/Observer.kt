package sample.observe

import sample.observe.Observable

interface Observer {
    fun update(observable: Observable)
}