package sample.controller;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;
import sample.model.ObjectManager;
import sample.model.StoredObjectManager;

public class Controller {
    private ObjectManager model;
    @FXML
    private MenuItem saveButton;

    public void setModel(ObjectManager model) {
        this.model = model;
    }

    @FXML
    public void initialize() {
        saveButton.addEventHandler(ActionEvent.ACTION, (event) -> {
            ((StoredObjectManager) model).saveObjects();
        });
    }


}
