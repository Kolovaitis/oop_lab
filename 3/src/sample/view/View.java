package sample.view;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import sample.*;
import sample.classes.Food;
import sample.model.ObjectManager;
import sample.observe.Observable;
import sample.observe.Observer;

import java.util.List;

public class View implements Observer {
private Parent root;
private ObjectManager model;
    public View(Parent root, ObjectManager model) {
        this.root = root;
        this.model = model;
        loadTypes();
    }

    @Override
    public void update(Observable observable) {
        updateObjects();
    }
    private void updateObjects(){
        VBox layout = (VBox)root.lookup("#objects");
        ObservableList<Node> children = layout.getChildren();
        children.clear();
        for(Food object:model.getAllObjects()){
            children.add(ViewCreator.createObjectItem(object, model));
        }
    }
    private void loadTypes(){
        MenuBar menuBar = (MenuBar)root.lookup("#menu");
        Menu addMenu = new Menu("Добавить объект");
        List<MenuItem> items= addMenu.getItems();
        for(Type type:Type.values()){
            MenuItem item = new MenuItem(type.getName());
            item.setOnAction(actionEvent -> {
                model.addObject(type.create());
            });
            items.add(item);
        }
        menuBar.getMenus().add(addMenu);
    }
}
