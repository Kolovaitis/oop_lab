package sample.view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Label;
import sample.Main;
import sample.classes.Food;
import sample.model.ObjectManager;

import java.io.IOException;

public class ViewCreator {
    public static Node createObjectItem(Food item, ObjectManager model) {
        try {
            Node itemNode = FXMLLoader.load(Main.class.getResource("layout/object_item.fxml"));
            ((Label) itemNode.lookup("#name")).textProperty().set(item.getName());
            ((Label) itemNode.lookup("#type")).textProperty().set(item.getType().getName());
            itemNode.setDisable(item.isChoosed());
            itemNode.setOnMouseClicked(mouseEvent -> {
                model.setChoosedObject(item);
            });
            return itemNode;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
