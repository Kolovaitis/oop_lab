package sample.view;

import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.layout.Pane;
import sample.classes.Food;
import sample.model.ObjectManager;
import sample.observe.Observable;
import sample.observe.Observer;

public class FieldsView implements Observer {
    private ObjectManager model;
    private Pane container;
    private View itemsView;


    public FieldsView(Pane container, ObjectManager model, View itemsView) {
        this.model = model;
        this.container = container;
        this.itemsView = itemsView;
    }

    @Override
    public void update(Observable observable) {

        ObservableList<Node> children = container.getChildren();
        children.clear();
        Food item = model.getChoosedObject();
        if(item!=null){
            children.add(item.getFieldsCreator().createFields(model, itemsView));
        }

    }
}
