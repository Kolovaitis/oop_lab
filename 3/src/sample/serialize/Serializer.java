package sample.serialize;

import sample.classes.Food;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public interface Serializer {
    void serialize(List<Food> objects, OutputStream outputStream);
    List<Food> deserialize(InputStream inputStream);
}
