package sample.serialize;

import sample.classes.Food;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class BinarySerializer implements Serializer {
    @Override
    public void serialize(List<Food> objects, OutputStream outputStream) {
        try {
        ObjectOutputStream oos = new ObjectOutputStream(outputStream);
        for (Object obj : objects) {
                oos.writeObject(obj);
        }
        oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Food> deserialize(InputStream inputStream) {
        List<Food> objects = new ArrayList<>();
        try {
            ObjectInputStream ois = new ObjectInputStream(inputStream);
            Object item;
            while ((item = ois.readObject()) != null) {
                objects.add((Food)item);
            }

            ois.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return objects;
    }
}
