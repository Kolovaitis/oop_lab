package sample.serialize;

import org.bson.*;
import org.bson.types.BasicBSONList;
import sample.classes.Food;
import sample.classes.Pizza;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class BSONSerializer implements Serializer {
    @Override
    public void serialize(List<Food> objects, OutputStream outputStream) {
        BSONEncoder encoder = new BasicBSONEncoder();
        BasicBSONObject list = new BasicBSONObject();
        for(Food food:objects){
            list.put(food.toString(), food);
        }
        try {
            byte[]encoded = encoder.encode(list);
            outputStream.write(encoded);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        encoder.done();
    }

    @Override
    public List<Food> deserialize(InputStream inputStream) {
        BSONDecoder decoder = new BasicBSONDecoder();
        BasicBSONObject list = null;
        List<Food>returnList = new ArrayList<>();
        try {
            list = (BasicBSONObject) decoder.readObject(inputStream);
            for(String key:list.keySet()){
                returnList.add((Food) list.get(key));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return returnList;
    }
}
