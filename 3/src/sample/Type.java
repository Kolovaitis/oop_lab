package sample;

import sample.classes.*;
import sample.creator.Creator;

public enum Type {
    PIZZA(false, "Пицца","pizza", (type)->new Pizza(type)),
    VEGETABLE( true,"Овощ", "vegetable", (type)->new Vegetable(type)),
    MEAT(true, "Мясо", "meat", (type)->new Meat(type)),
    SOUS( true,"Соус", "sous", (type)->new Sous(type)),
    CHEESE( true,"Сыр", "cheese", (type)->new Cheese(type));

    private boolean isIngridient;
    private String name;
    private String id;
    private Creator creator;

    Type(boolean isIngridient, String name, String id, Creator creator) {
        this.name = name;
        this.creator = creator;
        this.id = id;
        this.isIngridient = isIngridient;
    }



    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    public Food create(){
        return creator.create(this);
    }


    public boolean isIngridient() {
    return isIngridient;
    }
}
